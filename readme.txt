=============================================
  Symja Library - Java Symbolic Math System
=============================================

Features:
* arbitrary precision integers, rational and complex numbers
* differentiation, integration
* polynomials
* pattern matching
* linear algebra

Online demo: 
* http://symjaweb.appspot.com/

See the Wiki pages:
* https://bitbucket.org/axelclk/symja_android_library/wiki
	
axelclk_AT_gmail_DOT_com 