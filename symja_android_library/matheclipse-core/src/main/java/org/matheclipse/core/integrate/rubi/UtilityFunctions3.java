package org.matheclipse.core.integrate.rubi;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi.UtilityFunctions.*;

import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;
/** 
 * UtilityFunctions rules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class UtilityFunctions3 { 
  public static IAST RULES = List( 
SetDelayed(SimpProduct(Power($p(u),$p(m,true)),Power($p(v),$p(n,true))),
    Condition(Module(List(Set($s("gcd"),GCD(m,n)),w),CompoundExpression(Set(w,Expand(Times(Power(u,Times(m,Power($s("gcd"),CN1))),Power(v,Times(n,Power($s("gcd"),CN1)))))),Condition(SimpAux(Power(w,$s("gcd"))),Or(SqrtNumberQ(w),SqrtNumberSumQ(w))))),And(And(And(And(And(And(RationalQ(List(m,n)),Greater(m,C0)),Greater(n,C0)),SqrtNumberSumQ(u)),SqrtNumberSumQ(v)),PositiveQ(u)),PositiveQ(v)))),
SetDelayed(SimpProduct(Power($p(u),$p(m)),Power($p(v),$p(n,true))),
    Condition(Module(List(Set($s("gcd"),GCD(m,n)),w),CompoundExpression(Set(w,Simp(Times(Power(u,Times(m,Power($s("gcd"),CN1))),Power(v,Times(n,Power($s("gcd"),CN1)))))),Condition(SimpAux(Power(w,$s("gcd"))),Or(SqrtNumberQ(w),SqrtNumberSumQ(w))))),And(And(And(And(And(And(RationalQ(List(m,n)),Less(m,C0)),Greater(n,C0)),SqrtNumberSumQ(u)),SqrtNumberSumQ(v)),PositiveQ(u)),PositiveQ(v)))),
SetDelayed(SimpAux(Power($p(u),$p(n))),
    Condition(Times(Power(CN1,n),SimpAux(Power(Map($s("Minus"),u),n))),And(And(SumQ(u),IntIntegerQ(n)),Less(NumericFactor(Part(u,C1)),C0)))),
SetDelayed(SimpProduct($p(u),$p(v)),
    Condition(Times(CN1,SimpAux(Times(Map($s("Minus"),u),v))),And(And(SumQ(u),Less(NumericFactor(Part(u,C1)),C0)),Not(And(SqrtNumberSumQ(u),SqrtNumberSumQ(v)))))),
SetDelayed(SimpAux(Power($p(u),$p(n))),
    Condition(Module(List(Set($s("lst"),CommonFactors(Apply($s("List"),u)))),Condition(Times(Simp(Power(Part($s("lst"),C1),n)),SimpAux(Power(Apply($s("Plus"),Rest($s("lst"))),n))),UnsameQ(Part($s("lst"),C1),C1))),And(SumQ(u),IntIntegerQ(n)))),
SetDelayed(SimpAux(Power($p(u),$p(n))),
    Condition(Module(List(Set($s("lst"),CommonNumericFactors(Apply($s("List"),u)))),Condition(Times(Power(Part($s("lst"),C1),n),SimpAux(Power(Apply($s("Plus"),Rest($s("lst"))),n))),UnsameQ(Part($s("lst"),C1),C1))),And(And(SumQ(u),Not(IntIntegerQ(n))),Not(SqrtNumberSumQ(u))))),
SetDelayed(SimpProduct(Power(Plus($p(a),$p(b)),$p(m)),Power(Plus($p(c),$p(d)),$p(n,true))),
    Condition(Simp(Times(Power(Plus(a,b),Plus(m,n)),Power(Times(d,Power(b,CN1)),n))),And(And(IntIntegerQ(n),ZeroQ(Plus(Times(a,d),Times(CN1,Times(b,c))))),Not(SqrtNumberSumQ(Plus(a,b)))))),
SetDelayed(SimpProduct(Power($p(u),$p(m,true)),Power($p(v),$p(n,true))),
    Condition(Times(Power(CN1,n),Power(u,Plus(m,n))),And(And(And(IntIntegerQ(n),ZeroQ(Plus(u,v))),Not(RationalQ(u))),Or(Not(IntIntegerQ(m)),LessEqual(SmartLeafCount(u),SmartLeafCount(v)))))),
SetDelayed(SimpProduct(Power(Plus($p(a),$p(b)),$p(n,true)),Power(Plus($p(c),$p(d)),$p(n,true))),
    Condition(Simp(Power(Plus(Power(a,C2),Times(CN1,Power(b,C2))),n)),And(And(ZeroQ(Plus(a,Times(CN1,c))),ZeroQ(Plus(b,d))),IntIntegerQ(n)))),
SetDelayed(SimpSum(Times(Power($p(u),CN1),$p(a)),Times(Power($p(u),CN1),$p(b))),
    Condition(C1,SameQ(Plus(a,b),u))),
SetDelayed(SimpSum($p(a),Times(Power(Plus($p(c),$p(d)),C2),$p(b,true))),
    Condition(Simp(Times(Times(b,d),Plus(Times(C2,c),d))),ZeroQ(Plus(a,Times(b,Power(c,C2)))))),
SetDelayed(SimpSum($p(a),Times(Plus($p(c,true),$p(d)),Power(Plus($p(e),$p(f)),CN1),$p(b,true))),
    Condition(SimpAux(Times(ContentFactor(Plus(Times(a,e),Times(b,c))),Power(Plus(e,f),CN1))),And(And(And(And(And(And(NonsumQ(a),NonsumQ(b)),NonsumQ(c)),NonsumQ(d)),NonsumQ(e)),NonsumQ(f)),ZeroQ(Plus(Times(a,f),Times(b,d)))))),
SetDelayed(SimpProduct(Power($p(v),$p(m,true)),Power(Plus(Times(Power($p(v),$p(n)),$p(b,true)),$p(a)),$p(p,true))),
    Condition(SimpAux(Times(Power(v,Plus(m,Times(n,p))),Simp(Power(Plus(Times(a,Power(v,Times(CN1,n))),b),p)))),And(And(And(IntIntegerQ(p),RationalQ(List(m,n))),Less(n,C0)),Not(SqrtNumberSumQ(Plus(a,Times(b,Power(v,n)))))))),
SetDelayed(SimpProduct(Power($p(c),$p(m,true)),Power(Plus(Times(Power($p(c),$p(p,true)),$p(a,true)),Times(Power($p(c),$p(q,true)),$p(b,true))),$p(n,true))),
    Condition(SimpAux(Power(Plus(Times(a,Power(c,Plus(p,Times(m,Power(n,CN1))))),Times(b,Power(c,Plus(q,Times(m,Power(n,CN1)))))),n)),And(IntIntegerQ(n),RationalQ(List(m,p,q))))),
SetDelayed(SimpSum(Times(Power($p(n),CN1D2),$p(u,true)),Times(Power($p(n),C1D2),$p(v,true))),
    Condition(Times(Plus(C1,Times(n,Times(NumericFactor(v),Power(NumericFactor(u),CN1)))),Times(u,Power(Sqrt(n),CN1))),And(RationalQ(n),SameQ(NonnumericFactors(u),NonnumericFactors(v))))),
SetDelayed(SimpSum($p(u),Times(Plus($p(a),$p(b)),$p(v))),
    Condition(Module(List(Set($s("tmp"),SimpAux(Times(v,a)))),Condition(SimpAux(Plus(Times(Plus(C1,Times(NumericFactor($s("tmp")),Power(NumericFactor(u),CN1))),u),SimpAux(Times(v,b)))),SameQ(NonnumericFactors($s("tmp")),NonnumericFactors(u)))),And(And(NonsumQ(a),NonsumQ(u)),NonsumQ(v)))),
SetDelayed(SimpSum(Times(Plus($p(a),$p(b)),$p(u,true)),Times(Plus($p(c),$p(d)),$p(v))),
    Condition(Module(List(Set($s("tmp1"),SimpAux(Times(v,c))),Set($s("tmp2"),SimpAux(Times(u,a)))),Condition(CompoundExpression(Set($s("tmp1"),Times(NumericFactor($s("tmp1")),Power(NumericFactor($s("tmp2")),CN1))),If(Or(IntIntegerQ($s("tmp1")),Less(Less(integer(-2L),$s("tmp1")),C0)),SimpAux(Plus(Times(u,Plus(Times(Plus(C1,$s("tmp1")),a),b)),SimpAux(Times(v,d)))),SimpAux(Plus(SimpAux(Times(u,b)),Times(v,Plus(Times(Plus(C1,Power($s("tmp1"),CN1)),c),d)))))),SameQ(NonnumericFactors($s("tmp1")),NonnumericFactors($s("tmp2"))))),And(And(And(NonsumQ(a),NonsumQ(c)),NonsumQ(u)),NonsumQ(v)))),
SetDelayed(SimpAux(Power(E,Times(Plus(Times(Log($p(v)),$p(a,true)),$p(b)),$p(c,true)))),
    Times(SimpAux(Power(v,Times(a,c))),SimpAux(Power(E,Times(b,c))))),
SetDelayed(SimpAux(Power(E,Times(ArcTanh($p(v)),$p(n)))),
    Condition(Simp(Power(Plus(CN1,Times(C2,Power(Plus(C1,Times(CN1,v)),CN1))),Times(C1D2,n))),EvenQ(n))),
SetDelayed(SimpProduct(Power(E,Times(ArcTanh($p(v)),$p(n,true))),Power(Plus(C1,$p(w)),$p(m))),
    Condition(Simp(Times(Power(Plus(C1,Times(CN1,v)),Plus(m,Times(CN1,Times(C1D2,n)))),Power(Plus(C1,v),Plus(m,Times(C1D2,n))))),And(And(OddQ(n),HalfIntegerQ(m)),ZeroQ(Plus(Power(v,C2),w))))),
SetDelayed(SimpAux(Power(E,Times(ArcCoth($p(v)),$p(n)))),
    Condition(Simp(Power(Plus(C1,Times(CN1,Times(C2,Power(Plus(C1,Times(CN1,v)),CN1)))),Times(C1D2,n))),EvenQ(n))),
SetDelayed(SimpProduct(Power(E,Times(ArcCoth($p(v)),$p(n,true))),Power(Plus(C1,$p(w)),$p(m))),
    Condition(Simp(Times(Power(Plus(CN1,v),Plus(m,Times(CN1,Times(C1D2,n)))),Times(Power(Plus(C1,v),Plus(m,Times(C1D2,n))),Power(Power(v,Times(C2,m)),CN1)))),And(And(OddQ(n),HalfIntegerQ(m)),ZeroQ(Plus(Power(v,C2),Power(w,CN1)))))),
SetDelayed(SimpAux(Power(E,Times(ProductLog($p(v)),$p(n,true)))),
    Condition(Simp(Times(Power(v,n),Power(Power(ProductLog(v),n),CN1))),And(IntIntegerQ(n),Greater(n,C0)))),
SetDelayed(SimpSum(Times(Power(Cos($p(z)),C2),$p(u,true)),Times(Power(Sin($p(z)),C2),$p(v,true))),
    Condition(u,SameQ(u,v))),
SetDelayed(SimpSum(Times(Power(Sec($p(z)),C2),$p(u,true)),Times(Power(Tan($p(z)),C2),$p(v,true))),
    Condition(u,SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum(Times(Power(Csc($p(z)),C2),$p(u,true)),Times(Power(Cot($p(z)),C2),$p(v,true))),
    Condition(u,SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Sin($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Cos(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Cos($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Sin(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Tan($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Sec(z),C2)),SameQ(u,v))),
SetDelayed(SimpSum($p(u),Times(Power(Cot($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Csc(z),C2)),SameQ(u,v))),
SetDelayed(SimpSum($p(u),Times(Power(Sec($p(z)),C2),$p(v,true))),
    Condition(Times(v,Power(Tan(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Csc($p(z)),C2),$p(v,true))),
    Condition(Times(v,Power(Cot(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpAux(Power(Plus(Times(Cos($p(v)),$p(a,true)),Times(Sin($p(v)),$p(b,true))),$p(n))),
    Condition(SimpAux(Power(Plus(Times(Cos(v),Power(a,CN1)),Times(Sin(v),Power(b,CN1))),Times(CN1,n))),And(And(IntIntegerQ(n),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Power(b,C2)))))),
SetDelayed(SimpSum(Times(Power(Cosh($p(z)),C2),$p(u,true)),Times(Power(Sinh($p(z)),C2),$p(v,true))),
    Condition(u,SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum(Times(Power(Sech($p(z)),C2),$p(u,true)),Times(Power(Tanh($p(z)),C2),$p(v,true))),
    Condition(u,SameQ(u,v))),
SetDelayed(SimpSum(Times(Power(Coth($p(z)),C2),$p(u,true)),Times(Power(Csch($p(z)),C2),$p(v,true))),
    Condition(u,SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Sinh($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Cosh(z),C2)),SameQ(u,v))),
SetDelayed(SimpSum($p(u),Times(Power(Cosh($p(z)),C2),$p(v,true))),
    Condition(Times(v,Power(Sinh(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Tanh($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Sech(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Coth($p(z)),C2),$p(v,true))),
    Condition(Times(v,Power(Csch(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Sech($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Tanh(z),C2)),SameQ(u,Times(CN1,v)))),
SetDelayed(SimpSum($p(u),Times(Power(Csch($p(z)),C2),$p(v,true))),
    Condition(Times(u,Power(Coth(z),C2)),SameQ(u,v))),
SetDelayed(SimpAux(Power(Plus(Times(Cosh($p(v)),$p(a,true)),Times(Sinh($p(v)),$p(b,true))),$p(n))),
    Condition(SimpAux(Power(Plus(Times(Cosh(v),Power(a,CN1)),Times(CN1,Times(Sinh(v),Power(b,CN1)))),Times(CN1,n))),And(And(IntIntegerQ(n),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
SetDelayed(SimpSum(Times(Power($($p(f),$p(a)),$p(n,true)),$p(u,true)),Times(Power($($p(f),$p(a)),$p(n,true)),$p(v,true))),
    Condition(SimpAux(Times(Simp(Simplify(Plus(u,v))),Power($(f,a),n))),MemberQ(List($s("Erf"),$s("Erfc"),$s("Erfi"),$s("FresnelS"),$s("FresnelC"),$s("ExpIntegralEi"),$s("SinIntegral"),$s("CosIntegral"),$s("SinhIntegral"),$s("CoshIntegral"),$s("LogIntegral")),f))),
SetDelayed(SimpSum(Times(Power($($p(f),$p(a),$p(b)),$p(n,true)),$p(u,true)),Times(Power($($p(f),$p(a),$p(b)),$p(n,true)),$p(v,true))),
    Condition(SimpAux(Times(Simp(Simplify(Plus(u,v))),Power($(f,a,b),n))),MemberQ(List($s("Int"),$s("Gamma"),$s("PolyLog"),$s("EllipticF"),$s("EllipticE")),f))),
SetDelayed(ExpandIntegrandQ($p(m),$p(n),$p(p)),
    And(And(And(IntIntegerQ(p),Greater(p,C0)),NonzeroQ(Plus(Plus(m,Times(CN1,n)),C1))),If(ZeroQ(Plus(n,Times(CN1,C1))),Or(Or(Not(IntIntegerQ(m)),And(Less(m,C0),Not(LessEqual(LessEqual(Plus(Plus(m,p),C2),C0),Plus(Plus(m,Times(C2,p)),C2))))),LessEqual(p,Plus(m,C2))),Or(Or(Equal(p,C2),Not(IntIntegerQ(Times(Plus(m,C1),Power(n,CN1))))),And(Not(And(Less(C0,Times(Plus(m,C1),Power(n,CN1))),LessEqual(Times(Plus(m,C1),Power(n,CN1)),C3))),Not(LessEqual(Times(Plus(m,C1),Power(n,CN1)),Times(CN1,Plus(p,C1))))))))),
SetDelayed(ExpnExpand($p(u),$p(x,SymbolHead)),
    ExpnExpandAux(ExpandExpression(u,x),x)),
SetDelayed(ExpnExpandAux(Plus(Times(Power($p(x),CN1),$p(e,true)),Times(Power(Plus(Times($p(d,true),$p(x)),$p(c)),CN1),$p(f,true)),$p(u,true)),$p(x,SymbolHead)),
    Condition(Plus(ExpnExpandAux(u,x),Times(c,Times(e,Power(Times(x,Plus(c,Times(d,x))),CN1)))),And(FreeQ(List(c,d,e,f),x),ZeroQ(Plus(Times(d,e),f))))),
SetDelayed(ExpnExpandAux(Plus(Times(Power(Plus(Times($p(b,true),$p(x)),$p(a)),CN1),$p(e,true)),Times(Power(Plus(Times($p(d,true),$p(x)),$p(c)),CN1),$p(f,true)),$p(u,true)),$p(x,SymbolHead)),
    Condition(Plus(ExpnExpandAux(u,x),Times(Plus(Times(c,e),Times(a,f)),Power(Plus(Times(a,c),Times(Times(b,d),Power(x,C2))),CN1))),And(And(FreeQ(List(a,b,c,d,e,f),x),ZeroQ(Plus(Times(d,e),Times(b,f)))),ZeroQ(Plus(Times(b,c),Times(a,d)))))),
SetDelayed(ExpnExpandAux($p(u),$p(x,SymbolHead)),
    u),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power($p(x),$p(p,true)),$p(a,true)),Times(Power($p(x),$p(q,true)),$p(b,true))),$p(n)),$p(u,true)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(Times(u,Power(x,Times(n,p))),Power(Plus(a,Times(b,Power(x,Plus(q,Times(CN1,p))))),n)),x),And(And(FreeQ(List(a,b),x),IntIntegerQ(List(n,p,q))),GreaterEqual(Plus(q,Times(CN1,p)),C0)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power($p(c),$p(m)),$p(d,true)),Times($p(b,true),$p(v)),$p(a,true)),$p(p)),$p(u,true)),$p(x,SymbolHead)),
    Condition(Module(List($s("tmp")),ReplaceAll(ExpandExpression(Times(u,Power(Plus(Plus(a,Times(d,$s("tmp"))),Times(b,v)),p)),x),List(Rule($s("tmp"),Power(c,m))))),And(And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(p)),Less(p,C0)),FractionQ(m)),Not(FreeQ(v,x))))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power($p(c),$p(m)),$p(b,true),$p(v)),$p(a,true)),$p(p)),$p(u,true)),$p(x,SymbolHead)),
    Condition(Module(List($s("tmp")),ReplaceAll(ExpandExpression(Times(u,Power(Plus(a,Times(Times(b,$s("tmp")),v)),p)),x),List(Rule($s("tmp"),Power(c,m))))),And(And(And(And(FreeQ(List(a,b,c),x),IntIntegerQ(p)),Less(p,C0)),FractionQ(m)),Not(FreeQ(v,x))))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power($p(x),$p(n)),$p(b,true)),$p(a)),$p(p)),Power(Plus(Times(Power($p(x),$p(m,true)),$p(d,true)),$p(c,true)),$p(q,true))),$p(x,SymbolHead)),
    Condition(Module(List($s("aa"),$s("bb")),RegularizeTerm(ReplaceAll(Apart(Times(Power(Plus(c,Times(d,Power(x,m))),q),Power(Plus($s("aa"),Times($s("bb"),Power(x,n))),p)),x),List(Rule($s("aa"),a),Rule($s("bb"),b))),x)),And(And(And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n,p,q))),Less(p,C0)),Greater(n,C1)),Or(GreaterEqual(m,n),Less(m,C0))),Greater(q,C0)))),
SetDelayed(ExpandExpression(Times(Plus(Times(Power($p(x),$p(n,true)),$p(b,true)),$p(a)),Power(Plus(Times(Power($p(x),$p(n,true)),$p(d,true)),$p(c)),CN1),Power($p(x),$p(m))),$p(x,SymbolHead)),
    Condition(Plus(Times(a,Times(Power(x,m),Power(c,CN1))),Dist(Times(Plus(Times(b,c),Times(CN1,Times(a,d))),Power(c,CN1)),ExpandExpression(Times(Power(x,Plus(m,n)),Power(Plus(c,Times(d,Power(x,n))),CN1)),x))),And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n))),Greater(n,C0)),Less(m,C0)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power($p(x),$p(k,true)),$p(b,true)),Times(Power($p(x),$p(j)),$p(c,true)),$p(a)),$p(n)),$p(u,true)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(u,Times(Power(Plus(Times(C1D2,b),Times(c,Power(x,k))),Times(C2,n)),Power(Power(c,n),CN1))),x),And(And(And(And(FreeQ(List(a,b,c,j,k),x),IntIntegerQ(List(n,k,j))),Equal(j,Times(C2,k))),Less(n,C0)),ZeroQ(Plus(Power(b,C2),Times(CN1,Times(Times(C4,a),c))))))),
SetDelayed(ExpandExpression(Power($p(u),$p(n)),$p(x,SymbolHead)),
    Condition(Power(Plus(Plus(Coefficient(u,x,C0),Times(Coefficient(u,x,C1),x)),Times(Coefficient(u,x,C2),Power(x,C2))),n),And(And(And(And(RationalQ(n),Less(n,C0)),IntPolynomialQ(u,x)),Equal(Exponent(u,x),C2)),Not(MatchQ(u,Condition(Plus(Plus($p(a,true),Times($p(b,true),x)),Times($p(c,true),Power(x,C2))),FreeQ(List(a,b,c),x))))))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power($p(x),C4),$p(b,true)),$p(a)),CN1),$p(u)),$p(x,SymbolHead)),
    Condition(Plus(Plus(Times(Plus(Coefficient(u,x,C0),Times(Coefficient(u,x,C2),Power(x,C2))),Power(Plus(a,Times(b,Power(x,C4))),CN1)),Times(Coefficient(u,x,C1),Times(x,Power(Plus(a,Times(b,Power(x,C4))),CN1)))),Times(Coefficient(u,x,C3),Times(Power(x,C3),Power(Plus(a,Times(b,Power(x,C4))),CN1)))),And(And(FreeQ(List(a,b),x),IntPolynomialQ(u,x)),Less(Exponent(u,x),C4)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times($p(b,true),$p(x)),$p(a,true)),$p(m,true)),Power(Plus(Times($p(d,true),$p(x)),$p(c)),$p(n))),$p(x,SymbolHead)),
    Condition(Map(Function(RegularizeSubst(Slot1,x,Plus(a,Times(b,x)))),Apart(Times(Power(x,m),Power(Plus(Plus(c,Times(CN1,Times(a,Times(d,Power(b,CN1))))),Times(Times(d,Power(b,CN1)),x)),n)),x)),And(And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n))),Greater(m,C0)),Less(n,C0)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times($p(b,true),$p(x)),$p(a,true)),$p(n)),Power(Plus(Times($p(d,true),$p(x)),$p(c)),$p(n)),Power($p(x),$p(m,true))),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(Power(x,m),Power(Plus(Plus(Times(a,c),Times(Plus(Times(b,c),Times(a,d)),x)),Times(Times(b,d),Power(x,C2))),n)),x),And(And(FreeQ(List(a,b,c,d),x),IntIntegerQ(List(m,n))),Less(n,C0)))),
SetDelayed(ExpandExpression(Power(Sin($p(v)),$p(n)),$p(x,SymbolHead)),
    Condition(Expand(TrigReduce(Power(Sin(v),n)),x),And(IntIntegerQ(n),Greater(n,C1)))),
SetDelayed(ExpandExpression(Power(Cos($p(v)),$p(n)),$p(x,SymbolHead)),
    Condition(Expand(TrigReduce(Power(Cos(v),n)),x),And(IntIntegerQ(n),Greater(n,C1)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Cos($p(v)),C2),$p(d,true)),$p(c,true)),CN1),Power(Sin($p(v)),$p(n))),$p(x,SymbolHead)),
    Condition(Plus(Times(Times(CN1,Power(Sin(v),Plus(n,Times(CN1,C2)))),Power(d,CN1)),Dist(Times(Plus(c,d),Power(d,CN1)),ExpandExpression(Times(Power(Sin(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Cos(v),C2))),CN1)),x))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
SetDelayed(ExpandExpression(Times(Power(Cos($p(v)),$p(n)),Power(Plus(Times(Power(Sin($p(v)),C2),$p(d,true)),$p(c,true)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Times(CN1,Power(Cos(v),Plus(n,Times(CN1,C2)))),Power(d,CN1)),Dist(Times(Plus(c,d),Power(d,CN1)),ExpandExpression(Times(Power(Cos(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Sin(v),C2))),CN1)),x))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
SetDelayed(ExpandExpression(Times(Plus(Times(Power(Sin($p(v)),C2),$p(b,true)),$p(a)),Power(Plus(Times(Power(Cos($p(v)),C2),$p(d,true)),$p(c,true)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Times(CN1,b),Power(d,CN1)),Times(Plus(Times(b,c),Times(Plus(a,b),d)),Power(Times(d,Plus(c,Times(d,Power(Cos(v),C2)))),CN1))),FreeQ(List(a,b,c,d),x))),
SetDelayed(ExpandExpression(Times(Plus(Times(Power(Cos($p(v)),C2),$p(b,true)),$p(a)),Power(Plus(Times(Power(Sin($p(v)),C2),$p(d,true)),$p(c,true)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Times(CN1,b),Power(d,CN1)),Times(Plus(Times(b,c),Times(Plus(a,b),d)),Power(Times(d,Plus(c,Times(d,Power(Sin(v),C2)))),CN1))),FreeQ(List(a,b,c,d),x))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Sin($p(v)),$p(n,true)),$p(a,true)),Times(Power(Cos($p(v)),$p(n,true)),$p(b,true))),CN1),Power(Tan($p(v)),$p(n,true))),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Sec(v),n),Power(a,CN1)),Times(CN1,Times(b,Power(Times(a,Plus(Times(a,Power(Sin(v),n)),Times(b,Power(Cos(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Cot($p(v)),$p(n,true)),Power(Plus(Times(Power(Sin($p(v)),$p(n,true)),$p(a,true)),Times(Power(Cos($p(v)),$p(n,true)),$p(b,true))),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Csc(v),n),Power(b,CN1)),Times(CN1,Times(a,Power(Times(b,Plus(Times(a,Power(Sin(v),n)),Times(b,Power(Cos(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Cot($p(v)),$p(n,true)),$p(b,true)),$p(a)),CN1),Power(Sec($p(v)),$p(n,true))),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Sec(v),n),Power(a,CN1)),Times(CN1,Times(b,Power(Times(a,Plus(Times(a,Power(Sin(v),n)),Times(b,Power(Cos(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Csc($p(v)),$p(n,true)),Power(Plus(Times(Power(Tan($p(v)),$p(n,true)),$p(b,true)),$p(a)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Csc(v),n),Power(a,CN1)),Times(CN1,Times(b,Power(Times(a,Plus(Times(b,Power(Sin(v),n)),Times(a,Power(Cos(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Tan($p(v)),$p(b,true)),$p(a)),$p(n)),$p(u)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Power(Cos(v),C2),Power(a,CN1)),Times(Cos(v),Times(Sin(v),Power(b,CN1)))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Power(b,C2)))))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Cot($p(v)),$p(b,true)),$p(a)),$p(n)),$p(u)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Power(Sin(v),C2),Power(a,CN1)),Times(Cos(v),Times(Sin(v),Power(b,CN1)))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Power(b,C2)))))),
SetDelayed(ExpandExpression(Times(Cos($p(u)),Sin($p(u)),$p(v)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(v,Times(C1D2,Sin(Dist(C2,u)))),x),Or(MatchQ(v,Condition(Power(x,$p(m)),RationalQ(m))),MatchQ(v,Condition(Power($p(f),$p(w)),And(FreeQ(f,x),LinearQ(w,x))))))),
SetDelayed(ExpandExpression(Power(Sinh($p(v)),$p(n)),$p(x,SymbolHead)),
    Condition(Module(List(z),Expand(NormalForm(Subst(TrigReduce(Power(Sinh(z),n)),z,v),x),x)),And(IntIntegerQ(n),Greater(n,C1)))),
SetDelayed(ExpandExpression(Times(Power(Sinh($p(v)),$p(n,true)),Power($p(x),$p(m,true))),$p(x,SymbolHead)),
    Condition(Module(List(z),Expand(Times(Power(x,m),NormalForm(Subst(TrigReduce(Power(Sinh(z),n)),z,v),x)),x)),And(And(FreeQ(m,x),IntIntegerQ(n)),Greater(n,C0)))),
SetDelayed(ExpandExpression(Power(Cosh($p(v)),$p(n)),$p(x,SymbolHead)),
    Condition(Module(List(z),Expand(NormalForm(Subst(TrigReduce(Power(Cosh(z),n)),z,v),x),x)),And(IntIntegerQ(n),Greater(n,C1)))),
SetDelayed(ExpandExpression(Times(Power(Cosh($p(v)),$p(n,true)),Power($p(x),$p(m,true))),$p(x,SymbolHead)),
    Condition(Module(List(z),Expand(Times(Power(x,m),NormalForm(Subst(TrigReduce(Power(Cosh(z),n)),z,v),x)),x)),And(And(FreeQ(m,x),IntIntegerQ(n)),Greater(n,C0)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Cosh($p(v)),C2),$p(d,true)),$p(c,true)),CN1),Power(Sinh($p(v)),$p(n))),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Sinh(v),Plus(n,Times(CN1,C2))),Power(d,CN1)),Times(CN1,Dist(Times(Plus(c,d),Power(d,CN1)),ExpandExpression(Times(Power(Sinh(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Cosh(v),C2))),CN1)),x)))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
SetDelayed(ExpandExpression(Times(Power(Cosh($p(v)),$p(n)),Power(Plus(Times(Power(Sinh($p(v)),C2),$p(d,true)),$p(c,true)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Cosh(v),Plus(n,Times(CN1,C2))),Power(d,CN1)),Times(CN1,Dist(Times(Plus(c,Times(CN1,d)),Power(d,CN1)),ExpandExpression(Times(Power(Cosh(v),Plus(n,Times(CN1,C2))),Power(Plus(c,Times(d,Power(Sinh(v),C2))),CN1)),x)))),And(And(FreeQ(List(c,d),x),EvenQ(n)),Greater(n,C1)))),
SetDelayed(ExpandExpression(Times(Plus(Times(Power(Sinh($p(v)),C2),$p(b,true)),$p(a)),Power(Plus(Times(Power(Cosh($p(v)),C2),$p(d,true)),$p(c,true)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(b,Power(d,CN1)),Times(CN1,Times(Plus(Times(b,c),Times(CN1,Times(Plus(a,Times(CN1,b)),d))),Power(Times(d,Plus(c,Times(d,Power(Cosh(v),C2)))),CN1)))),FreeQ(List(a,b,c,d),x))),
SetDelayed(ExpandExpression(Times(Plus(Times(Power(Cosh($p(v)),C2),$p(b,true)),$p(a)),Power(Plus(Times(Power(Sinh($p(v)),C2),$p(d,true)),$p(c,true)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(b,Power(d,CN1)),Times(CN1,Times(Plus(Times(b,c),Times(CN1,Times(Plus(a,b),d))),Power(Times(d,Plus(c,Times(d,Power(Sinh(v),C2)))),CN1)))),FreeQ(List(a,b,c,d),x))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Sinh($p(v)),$p(n,true)),$p(a,true)),Times(Power(Cosh($p(v)),$p(n,true)),$p(b,true))),CN1),Power(Tanh($p(v)),$p(n,true))),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Sech(v),n),Power(a,CN1)),Times(CN1,Times(b,Power(Times(a,Plus(Times(a,Power(Sinh(v),n)),Times(b,Power(Cosh(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Coth($p(v)),$p(n,true)),Power(Plus(Times(Power(Sinh($p(v)),$p(n,true)),$p(a,true)),Times(Power(Cosh($p(v)),$p(n,true)),$p(b,true))),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Csch(v),n),Power(b,CN1)),Times(CN1,Times(a,Power(Times(b,Plus(Times(a,Power(Sinh(v),n)),Times(b,Power(Cosh(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Power(Coth($p(v)),$p(n,true)),$p(b,true)),$p(a)),CN1),Power(Sech($p(v)),$p(n,true))),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Sech(v),n),Power(a,CN1)),Times(CN1,Times(b,Power(Times(a,Plus(Times(a,Power(Sinh(v),n)),Times(b,Power(Cosh(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Csch($p(v)),$p(n,true)),Power(Plus(Times(Power(Tanh($p(v)),$p(n,true)),$p(b,true)),$p(a)),CN1)),$p(x,SymbolHead)),
    Condition(Plus(Times(Power(Csch(v),n),Power(a,CN1)),Times(CN1,Times(b,Power(Times(a,Plus(Times(b,Power(Sinh(v),n)),Times(a,Power(Cosh(v),n)))),CN1)))),And(FreeQ(List(a,b),x),IntIntegerQ(n)))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Tanh($p(v)),$p(b,true)),$p(a)),$p(n)),$p(u)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Power(Cosh(v),C2),Power(a,CN1)),Times(CN1,Times(Cosh(v),Times(Sinh(v),Power(b,CN1))))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
SetDelayed(ExpandExpression(Times(Power(Plus(Times(Coth($p(v)),$p(b,true)),$p(a)),$p(n)),$p(u)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(u,Power(Plus(Times(Times(CN1,Power(Sinh(v),C2)),Power(a,CN1)),Times(Cosh(v),Times(Sinh(v),Power(b,CN1)))),Times(CN1,n))),x),And(And(And(FreeQ(List(a,b),x),IntIntegerQ(n)),Less(n,C0)),ZeroQ(Plus(Power(a,C2),Times(CN1,Power(b,C2))))))),
SetDelayed(ExpandExpression(Times(Cosh($p(u)),Sinh($p(u)),$p(v)),$p(x,SymbolHead)),
    Condition(ExpandExpression(Times(v,Times(C1D2,Sinh(Dist(C2,u)))),x),Or(MatchQ(v,Condition(Power(x,$p(m)),RationalQ(m))),MatchQ(v,Condition(Power($p(f),$p(w)),And(FreeQ(f,x),LinearQ(w,x))))))),
SetDelayed(ExpandExpression(Times(Power($p(v),$p(n)),$p(u,true)),$p(x,SymbolHead)),
    Condition(Module(List(Set(w,ExpandExpression(u,x))),If(SumQ(w),Map(Function(RegularizeTerm(Times(Slot1,Power(v,n)),x)),w),Times(w,Power(v,n)))),And(FractionQ(n),Or(Less(n,C0),Greater(n,C1)))))
  );
}
