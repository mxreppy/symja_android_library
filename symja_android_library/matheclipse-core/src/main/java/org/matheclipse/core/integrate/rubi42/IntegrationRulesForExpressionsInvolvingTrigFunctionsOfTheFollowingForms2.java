package org.matheclipse.core.integrate.rubi42;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi42.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi42.UtilityFunctions.*;

import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;
/** 
 * IntegrationRules rules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class IntegrationRulesForExpressionsInvolvingTrigFunctionsOfTheFollowingForms2 { 
  public static IAST RULES = List( 
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Power(c,m),CN1),Int(Power(Times(c,Sin(Plus(a,Times(b,x)))),Plus(m,n)),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Power(c,m),CN1),Int(Power(Times(c,Cos(Plus(a,Times(b,x)))),Plus(m,n)),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,Times(CN1,C1D2))),Sqrt(Times(c,Sin(Plus(a,Times(b,x))))),Power(Sqrt(Sin(Plus(a,Times(b,x)))),CN1),Int(Power(Sin(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),PositiveIntegerQ(Plus(n,C1D2))))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,Times(CN1,C1D2))),Sqrt(Times(c,Cos(Plus(a,Times(b,x))))),Power(Sqrt(Cos(Plus(a,Times(b,x)))),CN1),Int(Power(Cos(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),PositiveIntegerQ(Plus(n,C1D2))))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,C1D2)),Sqrt(Sin(Plus(a,Times(b,x)))),Power(Sqrt(Times(c,Sin(Plus(a,Times(b,x))))),CN1),Int(Power(Sin(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),NegativeIntegerQ(Plus(n,Times(CN1,C1D2)))))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,C1D2)),Sqrt(Cos(Plus(a,Times(b,x)))),Power(Sqrt(Times(c,Cos(Plus(a,Times(b,x))))),CN1),Int(Power(Cos(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),NegativeIntegerQ(Plus(n,Times(CN1,C1D2)))))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Sin(Plus(a,Times(b,x)))),n),Power(Power(Sin(Plus(a,Times(b,x))),n),CN1),Int(Power(Sin(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))),Not(IntegerQ(Plus(n,Times(CN1,C1D2))))))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Cos(Plus(a,Times(b,x)))),n),Power(Power(Cos(Plus(a,Times(b,x))),n),CN1),Int(Power(Cos(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))),Not(IntegerQ(Plus(n,Times(CN1,C1D2))))))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Sin(Plus(a,Times(b,x)))),n),Power(Power(Sin(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Cos(Plus(a,Times(b,x))),m),Power(Sin(Plus(a,Times(b,x))),n)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Cos(Plus(a,Times(b,x)))),n),Power(Power(Cos(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Cos(Plus(a,Times(b,x))),n),Power(Sin(Plus(a,Times(b,x))),m)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Sin(Plus(a,Times(b,x)))),n),Power(Power(Sin(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Tan(Plus(a,Times(b,x))),m),Power(Sin(Plus(a,Times(b,x))),n)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Cos(Plus(a,Times(b,x)))),n),Power(Power(Cos(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Cot(Plus(a,Times(b,x))),m),Power(Cos(Plus(a,Times(b,x))),n)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Csc(Plus(a,Times(b,x))),n),Power(Times(c,Sin(Plus(a,Times(b,x)))),n),Int(Times(Power(Cot(Plus(a,Times(b,x))),m),Power(Power(Csc(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Sec(Plus(a,Times(b,x))),n),Power(Times(c,Cos(Plus(a,Times(b,x)))),n),Int(Times(Power(Tan(Plus(a,Times(b,x))),m),Power(Power(Sec(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Csc(Plus(a,Times(b,x))),n),Power(Times(c,Sin(Plus(a,Times(b,x)))),n),Int(Times(Power(Sec(Plus(a,Times(b,x))),m),Power(Power(Csc(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Sec(Plus(a,Times(b,x))),n),Power(Times(c,Cos(Plus(a,Times(b,x)))),n),Int(Times(Power(Csc(Plus(a,Times(b,x))),m),Power(Power(Sec(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(c,m),Int(Power(Times(c,Sin(Plus(a,Times(b,x)))),Plus(n,Times(CN1,m))),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(c,m),Int(Power(Times(c,Cos(Plus(a,Times(b,x)))),Plus(n,Times(CN1,m))),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,Times(CN1,C1D2))),Sqrt(Csc(Plus(a,Times(b,x)))),Sqrt(Times(c,Sin(Plus(a,Times(b,x))))),Int(Power(Csc(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),PositiveIntegerQ(Plus(n,C1D2))))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,Times(CN1,C1D2))),Sqrt(Sec(Plus(a,Times(b,x)))),Sqrt(Times(c,Cos(Plus(a,Times(b,x))))),Int(Power(Sec(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),PositiveIntegerQ(Plus(n,C1D2))))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,C1D2)),Power(Times(Sqrt(Csc(Plus(a,Times(b,x)))),Sqrt(Times(c,Sin(Plus(a,Times(b,x)))))),CN1),Int(Power(Csc(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),NegativeIntegerQ(Plus(n,Times(CN1,C1D2)))))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,C1D2)),Power(Times(Sqrt(Sec(Plus(a,Times(b,x)))),Sqrt(Times(c,Cos(Plus(a,Times(b,x)))))),CN1),Int(Power(Sec(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c),x),Not(IntegerQ(m))),NegativeIntegerQ(Plus(n,Times(CN1,C1D2)))))),
SetDelayed(Int(Times(Power(Times($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Csc(Plus(a,Times(b,x))),n),Power(Times(c,Sin(Plus(a,Times(b,x)))),n),Int(Power(Csc(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))),Not(IntegerQ(Plus(n,Times(CN1,C1D2))))))),
SetDelayed(Int(Times(Power(Times($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Sec(Plus(a,Times(b,x))),n),Power(Times(c,Cos(Plus(a,Times(b,x)))),n),Int(Power(Sec(Plus(a,Times(b,x))),Plus(m,n)),x)),And(And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))),Not(IntegerQ(Plus(n,Times(CN1,C1D2))))))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Tan(Plus(a,Times(b,x)))),n),Power(Power(Tan(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Sin(Plus(a,Times(b,x))),m),Power(Tan(Plus(a,Times(b,x))),n)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Cot(Plus(a,Times(b,x)))),n),Power(Power(Cot(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Cos(Plus(a,Times(b,x))),m),Power(Cot(Plus(a,Times(b,x))),n)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Cot(Plus(a,Times(b,x))),n),Power(Times(c,Tan(Plus(a,Times(b,x)))),n),Int(Times(Power(Cos(Plus(a,Times(b,x))),m),Power(Power(Cot(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Tan(Plus(a,Times(b,x))),n),Power(Times(c,Cot(Plus(a,Times(b,x)))),n),Int(Times(Power(Sin(Plus(a,Times(b,x))),m),Power(Power(Tan(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Power(c,m),CN1),Int(Power(Times(c,Tan(Plus(a,Times(b,x)))),Plus(m,n)),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Power(c,m),CN1),Int(Power(Times(c,Cot(Plus(a,Times(b,x)))),Plus(m,n)),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Tan(Plus(a,Times(b,x)))),n),Power(Power(Tan(Plus(a,Times(b,x))),n),CN1),Int(Power(Tan(Plus(a,Times(b,x))),Plus(m,n)),x)),And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Cot(Plus(a,Times(b,x)))),n),Power(Power(Cot(Plus(a,Times(b,x))),n),CN1),Int(Power(Cot(Plus(a,Times(b,x))),Plus(m,n)),x)),And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(c,m),Int(Power(Times(c,Tan(Plus(a,Times(b,x)))),Plus(n,Times(CN1,m))),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(c,m),Int(Power(Times(c,Cot(Plus(a,Times(b,x)))),Plus(n,Times(CN1,m))),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Cot(Plus(a,Times(b,x))),n),Power(Times(c,Tan(Plus(a,Times(b,x)))),n),Int(Power(Cot(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Tan(Plus(a,Times(b,x))),n),Power(Times(c,Cot(Plus(a,Times(b,x)))),n),Int(Power(Tan(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Tan(Plus(a,Times(b,x)))),n),Power(Power(Tan(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Sec(Plus(a,Times(b,x))),m),Power(Tan(Plus(a,Times(b,x))),n)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Times(c,Cot(Plus(a,Times(b,x)))),n),Power(Power(Cot(Plus(a,Times(b,x))),n),CN1),Int(Times(Power(Csc(Plus(a,Times(b,x))),m),Power(Cot(Plus(a,Times(b,x))),n)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§tan"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Cot(Plus(a,Times(b,x))),n),Power(Times(c,Tan(Plus(a,Times(b,x)))),n),Int(Times(Power(Csc(Plus(a,Times(b,x))),m),Power(Power(Cot(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§cot"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Tan(Plus(a,Times(b,x))),n),Power(Times(c,Cot(Plus(a,Times(b,x)))),n),Int(Times(Power(Sec(Plus(a,Times(b,x))),m),Power(Power(Tan(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Cos(Plus(a,Times(b,x))),n),Power(Times(c,Sec(Plus(a,Times(b,x)))),n),Int(Times(Power(Sin(Plus(a,Times(b,x))),m),Power(Power(Cos(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(Sin(Plus(a,Times(b,x))),n),Power(Times(c,Csc(Plus(a,Times(b,x)))),n),Int(Times(Power(Cos(Plus(a,Times(b,x))),m),Power(Power(Sin(Plus(a,Times(b,x))),n),CN1)),x)),FreeQ(List(a,b,c,m,n),x))),
SetDelayed(Int(Times(Power(Times($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(c,m),Int(Power(Times(c,Sec(Plus(a,Times(b,x)))),Plus(n,Times(CN1,m))),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n,true)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m,true))),$p(x,SymbolHead)),
    Condition(Times(Power(c,m),Int(Power(Times(c,Csc(Plus(a,Times(b,x)))),Plus(n,Times(CN1,m))),x)),And(FreeQ(List(a,b,c,n),x),IntegerQ(m)))),
SetDelayed(Int(Times(Power(Times($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,Times(CN1,C1D2))),Sqrt(Cos(Plus(a,Times(b,x)))),Sqrt(Times(c,Sec(Plus(a,Times(b,x))))),Int(Power(Cos(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c,m),x),Not(IntegerQ(m))),PositiveIntegerQ(Plus(n,C1D2))))),
SetDelayed(Int(Times(Power(Times($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,Times(CN1,C1D2))),Sqrt(Sin(Plus(a,Times(b,x)))),Sqrt(Times(c,Csc(Plus(a,Times(b,x))))),Int(Power(Sin(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c,m),x),Not(IntegerQ(m))),PositiveIntegerQ(Plus(n,C1D2))))),
SetDelayed(Int(Times(Power(Times($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,C1D2)),Power(Times(Sqrt(Cos(Plus(a,Times(b,x)))),Sqrt(Times(c,Sec(Plus(a,Times(b,x)))))),CN1),Int(Power(Cos(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c,m),x),Not(IntegerQ(m))),NegativeIntegerQ(Plus(n,Times(CN1,C1D2)))))),
SetDelayed(Int(Times(Power(Times($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(c,Plus(n,C1D2)),Power(Times(Sqrt(Sin(Plus(a,Times(b,x)))),Sqrt(Times(c,Csc(Plus(a,Times(b,x)))))),CN1),Int(Power(Sin(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c,m),x),Not(IntegerQ(m))),NegativeIntegerQ(Plus(n,Times(CN1,C1D2)))))),
SetDelayed(Int(Times(Power(Times($($s("§sec"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§cos"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(Cos(Plus(a,Times(b,x))),n),Power(Times(c,Sec(Plus(a,Times(b,x)))),n),Int(Power(Cos(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))),Not(IntegerQ(Plus(n,Times(CN1,C1D2))))))),
SetDelayed(Int(Times(Power(Times($($s("§csc"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(c)),$p(n)),Power($($s("§sin"),Plus(Times($p(b,true),$p(x)),$p(a,true))),$p(m))),$p(x,SymbolHead)),
    Condition(Times(Power(Sin(Plus(a,Times(b,x))),n),Power(Times(c,Csc(Plus(a,Times(b,x)))),n),Int(Power(Sin(Plus(a,Times(b,x))),Plus(m,Times(CN1,n))),x)),And(And(FreeQ(List(a,b,c,m,n),x),Not(IntegerQ(m))),Not(IntegerQ(Plus(n,Times(CN1,C1D2)))))))
  );
}
